<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$typeImageQr = '.jpg';
if ($_POST)
{
    //$str = urlencode(serialize($formFields));
    //$formFields = unserialize(urldecode($str));

    $zip = new ZipArchive();
    $filename = "qr_images/" . time() . ".zip";
    if ($zip->open($filename, ZipArchive::CREATE) !== TRUE)
    {
        exit("cannot open <$filename>\n");
    }



    for ($i = $_POST['desde']; $i <= $_POST['hasta']; $i++)
    {
        $nombreImagen = $_POST['letra'] . "-" . str_pad($i, 8, "0", STR_PAD_LEFT);

        //echo $nombreImagen . "<br>";
        //$url = "http://api.qrserver.com/v1/create-qr-code/?data={$nombreImagen}&size=500x500";
        //$textAdd = urlencode($nombreImagen . " \n uakika.com");
        $textAdd = urlencode($nombreImagen);
        $textAdd = '';
        $text = urlencode($nombreImagen);
        $url = "http://qrickit.com/api/qr?d={$nombreImagen}&addtext={$textAdd}&qrsize=500&t=j&e=h";
        if (copy($url, 'qr_images/' . $nombreImagen . $typeImageQr))
        {

            $im = imagecreatefromjpeg('qr_images/' . $nombreImagen . $typeImageQr);
            $color_texto = imagecolorallocate($im, 0, 0, 255);
            $negro = imagecolorallocate($im, 0, 0, 0);
            $font = 'fonts/quicksand/Quicksand-Regular.otf';
            $fontBold = 'fonts/quicksand/Quicksand-Bold.otf';
            $grey = imagecolorallocate($im, 128, 128, 128);
            $black = imagecolorallocate($im, 0, 0, 0);
            imagettftext($im, 28, 0, 127, 467, $grey, $font, $nombreImagen);
            imagettftext($im, 28, 0, 125, 465, $black, $font, $nombreImagen);

            imagettftext($im, 20, 0, 170, 490, $black, $fontBold, 'uakika.com');
            //imagestring($im, $fuente, 200, 450, $nombreImagen, $color_texto);
            imagejpeg($im, 'qr_images/' . $nombreImagen . $typeImageQr);
            imagedestroy($im);


            $urlEncode = urlencode($url);
            //$checkUrl = "http://api.qrserver.com/v1/read-qr-code/?fileurl=" . $urlEncode;
            $checkUrl = 'http://zxing.org/w/decode.jspx';
            //echo '<img src="'.$url.'" alt="" title="" /><br><br><br>';
            /*
            echo '<img src="qr_images/' . $nombreImagen . $typeImageQr . '" alt="" title="" />
			<br>
			<a href="' . $checkUrl . '" target="_blank">Check It!</a> uploading image file
			<br><br>';
             */

            if ($zip->addFile('qr_images/' . $nombreImagen . $typeImageQr))
            {
                //unlink('qr_images/' . $nombreImagen . $typeImageQr);
            }
        }
        else
        {
            echo "No se pudo copiar imagen<BR>";
        }
    }

    $zip->close();
    echo "Se generaron todos los archivos QR<br>Puede descargar el Zip file con las imagenes desde <a href='{$filename}'>aqu&iacute;</a>";
}
?>

<div style="width:500px;">
    <fieldset>
        <legend>Generador de Imagenes QR</legend>
        <form action="" method="POST">
            <label for="letra"> Ingrese Letra:</label>
            <input type="text" id="letra" name="letra" value="<?php echo $_POST['letra'] ?>"><br>
            <label for="desde"> Ingrese Numero Desde:</label>
            <input type="number" id="desde" name="desde" value="<?php echo $_POST['desde'] ?>"><br>
            <label for="hasta"> Ingrese Numero Hasta:</label>
            <input type="number" id="hasta" name="hasta" value="<?php echo $_POST['hasta'] ?>"><br>
            <input type = "submit" value="Generar">
            <form>
                </fieldset>
                </div>
